1.8 Distinguish between the terms fatal error and nonfatal error. Why might you prefer to experience a fatal error rather than a nonfatal error?


In case of FE the programme does not execute, but in case of NFE, the programme can still run, and make wrong results, and the programmer may be not aware about what exactly went wrong, unless he/she checks all the steps.
